<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta1(){
        $dataProvider = new ActiveDataProvider();

        
        $model = Query::find()->Select('nompuerto,dorsal')->from('puertos')->where('altura < 1000');
     
        return $this->render('query',[
            'campos'=>['nompuerto,dorsal'],
            'titulo'=>"Consulta1",
            'query'=>"Select nompuerto, dorsal from puertos where altura < 1000"      
        ]);
        
    }
     public function actionConsulta2(){
        $dataProvider = new ActiveDataProvider();

        
        $model= Query::find()->Select('numetapa,kms')->from('etapa')->where('llegada=salida');
     
        return $this->render('query',[
            'campos'=>['numetapa,kms'],
            'titulo'=>"Consulta2",
            'query'=>"Select numetapa,kms from etapa where llegada=salida"      
        ]);
        
    }
 public function actionConsulta3(){
        $dataProvider = new ActiveDataProvider();

        
        $model = Query::find()->Select('nompuerto,dorsal')->from('puertos')->where('altura < 1000');
     
        return $this->render('query',[
            'campos'=>['nompuerto,dorsal'],
            'titulo'=>"Consulta3",
            'query'=>"Select nompuerto, dorsal where altura > 1500"      
        ]);
        
    }
}



//consulta1 Select nompuerto, dorsal from puertos where altura < 1000


//consulta2 Select numetapa,kms from etapa where llegada=salida


//consulta3 Select nompuerto, dorsal where altura > 1500